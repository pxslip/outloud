module.exports = function (sequelize, DataTypes) {

  var User = sequelize.define('User', {
    email: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    user_id: DataTypes.STRING,
    full_name: DataTypes.STRING,
  }, {
    classMethods: {
      associate: function (models) {
        User.belongsToMany(models.Course, {through: models.Enrollment});
        User.hasMany(models.Recording);
      }
    }
  });

  return User;
};
