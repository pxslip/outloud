module.exports = function (sequelize, DataTypes) {

  var Recording = sequelize.define('Recording', {
    recording: DataTypes.STRING
  }, {
    classMethods: {
      associate: function (models) {
        Recording.belongsTo(models.User);
        Recording.belongsTo(models.Assignment);
      }
    }
  });

  return Recording;
};
