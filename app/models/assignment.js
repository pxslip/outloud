module.exports = function (sequelize, DataTypes) {

  var Assignment = sequelize.define('Assignment', {
    name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function (models) {
        Assignment.hasMany(models.Recording);
        Assignment.belongsTo(models.Course);
      }
    }
  });

  return Assignment;
};
