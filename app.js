//load the .env file
require('dotenv').load();
//outloud a voice assignment tool over LTI
var express = require('express'),
    db = require('./app/models'),
    port = process.env.PORT || 3000;

var app = express();

require('./config/express')(app);

db.sequelize
  .sync()
  .then(function () {
    app.listen(port || 3000, function () {
      console.log('Express server listening on port ' + port);
    });
  }).catch(function (e) {
    throw new Error(e);
  });
