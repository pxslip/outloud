var express = require('express'),
  router = express.Router(),
  db = require('../models');
  lti = require('ims-lti');

module.exports = function (app) {
  app.use('/lti', router);
};

router.get('/', function (req, res, next) {
  res.render('error', {
    message: 'LTI requests must come from a POST request'
  });
});

router.post('/', function (req, res, next) {
  //first get the consumer guid to look up the consumer
  db.Consumer.findOne({where: {key:req.body.oauth_consumer_key}}).then(function(consumer) {
    if(consumer === null) {
      res.redirect(req.body.launch_presentation_return_url);
    }
    else {
      if(!consumer.guid) {
        //update the guid if it's not already set
        //TODO why do I want to set the guid?
        consumer.update({guid:req.body.tool_consumer_instance_guid});
      }
      var provider = new lti.Provider(consumer.key, consumer.secret);//TODO figure out how to handle non SHA1 signatures
      provider.valid_request(req, function(err, is_valid) {
        if(!is_valid) {
          //TODO throw a proper error here
          console.log(err);
        }
        //get the user, course, and user's role in course
        db.User.findOrCreate({where: {user_id:provider.userId}, defaults:
          {
            email: provider.body.lis_person_contact_email_primary,
            first_name: provider.body.lis_person_name_given,
            last_name: provider.body.lis_person_name_family,
            full_name: provider.body.lis_person_name_full
          }})
          .spread(function(user, created) {
            // this approach means that the user will be created if they do not already exist
            db.Course.findOrCreate({where: {context_id: provider.body.context_id}}).spread(function(course, created) {
              res.render('lti/index', {
                user: user.get({plain:true}),
                course: course.get({plain:true})
              });
            });
          });

      });
    }
  });
});
