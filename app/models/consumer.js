module.exports = function (sequelize, DataTypes) {

  var Consumer = sequelize.define('Consumer', {
    guid: DataTypes.STRING,
    key: DataTypes.STRING,
    secret: DataTypes.STRING,
    name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function (models) {
        // example on how to add relations
        // Article.hasMany(models.Comments);
      }
    }
  });

  return Consumer;
};
