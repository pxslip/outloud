module.exports = function (sequelize, DataTypes) {

  var Course = sequelize.define('Course', {
    context_id: DataTypes.STRING
  }, {
    classMethods: {
      associate: function (models) {
        Course.belongsToMany(models.User, {through: models.Enrollment});
        Course.hasMany(models.Assignment);
      }
    }
  });

  return Course;
};
